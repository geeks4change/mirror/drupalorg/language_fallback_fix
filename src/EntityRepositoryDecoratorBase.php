<?php

namespace Drupal\language_fallback_fix;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;

class EntityRepositoryDecoratorBase implements EntityRepositoryInterface {

  /**
   * The decorated class.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $decorated;

  /**
   * EntityRepositoryDecorator constructor.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $decorated
   */
  public function __construct(EntityRepositoryInterface $decorated) {
    $this->decorated = $decorated;
  }

  /**
   * @inheritDoc
   */
  public function loadEntityByUuid($entity_type_id, $uuid) {
    return $this->decorated->loadEntityByUuid($entity_type_id, $uuid);
  }

  /**
   * @inheritDoc
   */
  public function loadEntityByConfigTarget($entity_type_id, $target) {
    return $this->decorated->loadEntityByConfigTarget($entity_type_id, $target);
  }

  /**
   * @inheritDoc
   */
  public function getTranslationFromContext(EntityInterface $entity, $langcode = NULL, $context = []) {
    return $this->decorated->getTranslationFromContext($entity, $langcode, $context);
  }

  /**
   * @inheritDoc
   */
  public function getActive($entity_type_id, $entity_id, array $contexts = NULL) {
    return $this->decorated->getActive($entity_type_id, $entity_id, $contexts);
  }

  /**
   * @inheritDoc
   */
  public function getActiveMultiple($entity_type_id, array $entity_ids, array $contexts = NULL) {
    return $this->decorated->getActiveMultiple($entity_type_id, $entity_ids, $contexts);
  }

  /**
   * @inheritDoc
   */
  public function getCanonical($entity_type_id, $entity_id, array $contexts = NULL) {
    return $this->decorated->getCanonical($entity_type_id, $entity_id, $contexts);
  }

  /**
   * @inheritDoc
   */
  public function getCanonicalMultiple($entity_type_id, array $entity_ids, array $contexts = NULL) {
    return $this->decorated->getCanonicalMultiple($entity_type_id, $entity_ids, $contexts);
  }
}
